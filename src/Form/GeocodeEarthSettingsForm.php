<?php

namespace Drupal\geocode_earth_autocomplete\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\key\KeyRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class GeocodeEarthSettingsForm
 *
 * @package Drupal\geocode_earth_autocomplete\Form
 */
class GeocodeEarthSettingsForm extends ConfigFormBase {

  /**
   * The key repository object.
   *
   * @var \Drupal\key\KeyRepository
   */
  protected $keyRepository;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory object.
   * @param \Drupal\key\KeyRepository $keyRepository
   *   Key repository object.
   */
  public function __construct(ConfigFactoryInterface $config_factory, KeyRepository $keyRepository) {
    parent::__construct($config_factory);
    $this->keyRepository = $keyRepository;
  }

  /**
   * Service injection.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Container object.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('key.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'geocode_earth_autocomplete.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'geocode_earth_autocomplete_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('geocode_earth_autocomplete.settings');

    $form['settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Mapbox settings'),
    ];

    $form['settings']['api_key_name'] = [
      '#type' => 'key_select',
      '#title' => $this->t('API Key'),
      '#description' => $this->t('API key to use Geocode Earth Autocomplete.'),
      '#empty_option' => $this->t('- Select Key -'),
      '#default_value' => $config->get('api_key_name'),
      '#key_filters' => ['type' => 'authentication'],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('geocode_earth_autocomplete.settings')
      ->set('api_key_name', $form_state->getValue('api_key_name'))
      ->save();
  }

}
