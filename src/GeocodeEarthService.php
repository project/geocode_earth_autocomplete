<?php

namespace Drupal\geocode_earth_autocomplete;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\key\KeyRepository;

/**
 * Geocode Earth service.
 */
class GeocodeEarthService {

  use LoggerChannelTrait;
  use StringTranslationTrait;

  /**
   * The config factory object.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The key repository object.
   *
   * @var \Drupal\key\KeyRepository
   */
  protected $keyRepository;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   Config factory object.
   * @param \Drupal\key\KeyRepository $keyRepository
   *   Key repository object.
   */
  public function __construct(ConfigFactory $config_factory, KeyRepository $keyRepository) {
    $this->configFactory = $config_factory;
    $this->keyRepository = $keyRepository;
  }

  /**
   * Get config.
   *
   * @return \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   *   Returns editable config object.
   */
  public function getConfig() {
    return $this->configFactory->getEditable('geocode_earth_autocomplete.settings');
  }

  /**
   * Get API key.
   *
   * @return mixed|string
   */
  public function getApiKey() {
    return $this->keyRepository->getKey($this->getConfig()->get('api_key_name'))->getKeyValue();
  }
}
