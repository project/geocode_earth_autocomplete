<?php

namespace Drupal\geocode_earth_autocomplete\Plugin\AddressProvider;

use Drupal\address_autocomplete\Plugin\AddressProviderBase;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Defines a Geocode Earth plugin for address_autocomplete.
 *
 * @AddressProvider(
 *   id = "geocode_earth",
 *   label = @Translation("Geocode Earth"),
 * )
 */
class GeocodeEarth extends AddressProviderBase {

  use StringTranslationTrait;

  /**
   * @inheritDoc
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'api_search_endpoint' => 'https://api.geocode.earth/v1/search',
    ];
  }

  /**
   * @inheritDoc
   */
  public function processQuery($string) {
    /** @var \Drupal\geocode_earth_autocomplete\GeocodeEarthService $geocodeEarthService */
    $geocodeEarthService = \Drupal::service('geocode_earth');
    $apiKey = $geocodeEarthService->getApiKey();

    // No API key, return here.
    if (empty($apiKey)) {
      return [];
    }

    // Get results from search endpoint.
    $url = $this->configuration['api_search_endpoint'] . '?api_key=' . $apiKey . '&text=' . $string;
    $response = $this->client->request('GET', $url);

    // Get response body.
    $content = Json::decode($response->getBody());

    // Process results.
    $results = [];
    foreach ($content["features"] as $key => $feature) {
      $results[$key]['street_name'] = $feature['properties']['name'];
      $results[$key]['town_name'] = $feature['properties']['county'];
      $results[$key]['zip_code'] = $feature['properties']['postalcode'];
      $results[$key]['administrative_area'] = $feature['properties']['region_a'];
      $results[$key]['label'] = $feature['properties']['label'];
    }

    // Return results.
    return $results;
  }

  /**
   * @inheritDoc
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['api_search_endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Geocode Earth Search Endpoint'),
      '#default_value' => $this->configuration['api_search_endpoint'],
      '#attributes' => [
        'autocomplete' => 'off',
      ],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * @inheritDoc
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $configuration = $this->getConfiguration();
    $configuration['api_search_endpoint'] = $form_state->getValue('api_search_endpoint');

    $this->setConfiguration($configuration);
  }

}
