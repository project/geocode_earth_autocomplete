<?php

namespace Drupal\geocode_earth_autocomplete\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Field\WidgetInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Locale\CountryManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'geocode_earth_autocomplete_field_widget' widget.
 *
 * @FieldWidget(
 *   id = "geocode_earth_autocomplete_field_widget",
 *   label = @Translation("Geocode Earth Autocomplete"),
 *   field_types = {
 *     "text",
 *     "string"
 *   }
 * )
 */
class GeocodeEarthAutocompleteFieldWidget extends WidgetBase implements WidgetInterface {

  /**
   * The country manager.
   *
   * @var \Drupal\Core\Locale\CountryManagerInterface
   */
  protected $countryManager;

  /**
   * Constructs Field Widget.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param array $third_party_settings
   *   Third party settings.
   * @param \Drupal\Core\Locale\CountryManagerInterface $country_manager
   *   Country Manager Service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, CountryManagerInterface $country_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->countryManager = $country_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('country_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'size' => 10,
        'country' => [],
        'layers' => [],
        'gid' => '',
        'language' => 'en',
        'placeholder' => '',
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];

    $countries = $this->countryManager->getStandardList();
    foreach ($countries as $key => $value) {
      $countries[$key] = $value->__toString();
    }
    $elements['country'] = [
      '#type' => 'select',
      '#multiple' => TRUE,
      '#size' => 10,
      '#title' => $this->t('Country'),
      '#options' => $countries,
      '#default_value' => $this->getSetting('country'),
      '#description' => $this->t('Sets a country filter to only return results in the specified country. Use a comma separated list of ISO 3166-1 country codes (2 or 3 letter variants).'),
    ];

    $layers = [
      'address' => $this->t('Address'),
      'venue' => $this->t('Venue'),
      'street' => $this->t('Streets'),
      'intersection' => $this->t('Intersection'),
      'continent' => $this->t('Continent'),
      'empire' => $this->t('Empire'),
      'country' => $this->t('Country'),
      'dependency' => $this->t('Dependency'),
      'disputed' => $this->t('Disputed'),
      'region' => $this->t('Region'),
      'county' => $this->t('County'),
      'localadmin' => $this->t('Local Admin'),
      'locality' => $this->t('Locality'),
      'borough' => $this->t('Borough'),
      'neighbourhood' => $this->t('Neighbourhood'),
    ];
    $elements['layers'] = [
      '#type' => 'select',
      '#multiple' => TRUE,
      '#size' => 10,
      '#title' => $this->t('Layers'),
      '#options' => $layers,
      '#default_value' => $this->getSetting('layers'),
      '#description' => $this->t('Filtering results by layer can be useful to return only particular types of results.'),
    ];

    $elements['gid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Restrict Results By Global Id'),
      '#default_value' => $this->getSetting('gid'),
      '#description' => $this->t('These IDs can be used to filter results to areas smaller than countries, and/or areas with shapes not well represented by rectangular bounding boxes.'),
    ];

    $elements['language'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Language'),
      '#default_value' => $this->getSetting('language'),
      '#description' => $this->t('Sets the language in which results are returned.'),
    ];

    $elements['size'] = [
      '#type' => 'number',
      '#title' => $this->t('Controls how many results should be shown.'),
      '#default_value' => $this->getSetting('size'),
      '#required' => TRUE,
      '#min' => 1,
      '#max' => 10,
    ];

    $elements['placeholder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Placeholder'),
      '#default_value' => $this->getSetting('placeholder'),
      '#description' => $this->t('Text that will be shown inside the field until a value is entered. This hint is usually a sample value or a brief description of the expected format.'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    // Country.
    if (!empty($this->getSetting('country'))) {
      $summary[] = $this->t('Countries: @country', ['@country' => implode(', ', $this->getSetting('country'))]);
    }

    // Global Id.
    if (!empty($this->getSetting('gid'))) {
      $summary[] = $this->t('Global Id: @gid', ['@gid' => $this->getSetting('gid')]);
    }

    // Layers.
    if (!empty($this->getSetting('layers'))) {
      $summary[] = $this->t('Layers: @layers', ['@layers' => implode(', ', $this->getSetting('layers'))]);
    }

    // Language.
    if (!empty($this->getSetting('language'))) {
      $summary[] = $this->t('Language: @language', ['@language' => $this->getSetting('language')]);
    }

    // Placeholder.
    if (!empty($this->getSetting('placeholder'))) {
      $summary[] = $this->t('Placeholder: @placeholder', ['@placeholder' => $this->getSetting('placeholder')]);
    }

    // Size.
    $summary[] = $this->t('Results shown: @size', ['@size' => $this->getSetting('size')]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['value'] = $element + [
      '#type' => 'textfield',
      '#theme' => 'geocode_earth_autocomplete',
      '#default_value' => isset($items[$delta]->value) ? $items[$delta]->value : NULL,
      '#maxlength' => $this->getFieldSetting('max_length'),
      '#ge_autocomplete_options' => [
        'size' => !empty($this->getSetting('size')) ? $this->getSetting('size') : 10,
        'country' => implode(',', $this->getSetting('country')),
        'gid' => $this->getSetting('gid'),
        'layers' => implode(',', $this->getSetting('layers')),
        'language' => !empty($this->getSetting('language')) ? $this->getSetting('language') : 'en',
        'placeholder' => $this->getSetting('placeholder'),
      ],
    ];
    return $element;
  }

}
