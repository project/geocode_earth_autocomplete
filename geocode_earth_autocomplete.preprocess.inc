<?php

/**
 * @file
 * Contains geocode_earth_autocomplete.preprocess.inc.
 */

/**
 * Prepares variables for geocode earth autocomplete template.
 *
 * Default template: geocode-earth-autocomplete.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_geocode_earth_autocomplete(array &$variables) {
  $variables['ge_api_key'] = NULL;
  $variables['ge_autocomplete_options'] = !empty($variables['element']['#ge_autocomplete_options']) ? $variables['element']['#ge_autocomplete_options'] : [];
  $variables['ge_autocomplete_value'] = !empty($variables['element']['#value']) ? $variables['element']['#value'] : '';
  $apiKeyName = \Drupal::configFactory()->get('geocode_earth_autocomplete.settings')->get('api_key_name');
  if (!empty($apiKeyName)) {
    /** @var \Drupal\key\KeyRepository $keyRepository */
    $keyRepository = \Drupal::service('key.repository');
    $variables['ge_api_key'] = $keyRepository->getKey($apiKeyName)->getKeyValue();
  }
}
