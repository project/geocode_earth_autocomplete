(function ($, Drupal, drupalSettings) {
  'use strict';
  Drupal.behaviors.geocode_earth_autocomplete = {
    attach: function (context, settings) {
      // Check and make sure api key is set.
      if (!drupalSettings.geocode_earth_autocomplete || !drupalSettings.geocode_earth_autocomplete.apiKey) {
        return;
      }
      // Loop over ge-autocomplete fields.
      $('ge-autocomplete', context).once('geocode_earth_autocomplete').each(function () {
        // Get autocomplete element.
        let ge_autocomplete = $(this);
        // Get drupal input.
        let drupal_input = null;
        let input_id = ge_autocomplete.data('input-id');
        if (input_id) {
          drupal_input = $('#' + input_id);
          // Hide drupal input.
          drupal_input.hide();
          // If drupal input has value, trigger change event.
          if (drupal_input.val()) {
            // Set placeholder to current value of input.
            ge_autocomplete.attr('placeholder', drupal_input.val());
          }
        }
        // 'select' event handler - when a user selects an item from the suggestions.
        ge_autocomplete.on('select', function (event) {
          if (drupal_input) {
            drupal_input.val(event.detail.properties.label);
          }
        });
        // 'change' event handler - on every keystroke as the user types.
        ge_autocomplete.on('change', function (event) {
        });
        // 'error' event handler - on error
        ge_autocomplete.on('error', function (event) {
          console.log(event.detail, event);
        });
      });
    }
  };
})(jQuery, Drupal, drupalSettings);
