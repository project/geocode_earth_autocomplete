## Geocode Earth: Autocomplete

Provides a field widget for text-fields to allow for the
Autocomplete element (Web Component) for use with the Geocode
Earth Autocomplete API.

### Requirements

Requires API key from Geocode Earth:
https://geocode.earth/docs/intro/authentication/

Requires the Key module:
https://www.drupal.org/project/key

Requires the Address module:
https://www.drupal.org/project/address

Requires the Address Autocomplete module:
https://www.drupal.org/project/address_autocomplete

### Documentation

https://geocode.earth/docs
https://geocode.earth/docs/components/autocomplete-element/

### Install/Usage

* Install and enable module like any other contributed module.
* Configure Geocode Earth API key: **/admin/config/geocode-earth-autocomplete**

### Configure Autocomplete Widget

* Configure or create new textfield field on Entity.
* Goto "Manage form display" and select the textfield field you wish to
  use. "Geocode Earth Autocomplete" widget should be available for
  selection.
* Configure any necessary settings for the field widget.
* Save form display settings.
* You should now be able to use the widget on entity add/edit forms.

### Configure Address Autocomplete Widget

* Configure address autocomplete settings here:
**/admin/config/address-autocomplete**
* Enable "Geocode Earth" provider and configure settings if needed.
* Configure or create new address field on Entity.
* Goto "Manage form display" and select the address field you wish to
  use. "Geocode Earth Address Autocomplete" widget should be available for
  selection.
* Save form display settings.
* Street address field should now be an autocomplete field. When selection
is made, other address fields will be populated.

### Maintainers

George Anderson (geoanders)
https://www.drupal.org/u/geoanders
